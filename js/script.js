// 1.Створити тег можна за допомогою document.createElement().
// 2.Перший параметр у element.insertAdjacentHTML визначає позицію елемента,
// є 4 варіанти, що можна прописати: 'beforebegin', 'afterbegin',
// 'beforeend', 'afterend'.
// 3.Видалити елемент можна за допомогою методу element.remove()


function search(items, parent = document.body) {

    let list = document.createElement('ul');

    parent.appendChild(list);
    console.log(parent);
    items.forEach((element) => {
        let li = document.createElement('li');
        list.appendChild(li);
        li.innerText = element;
    });

    console.log(items);
};

let list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let list2 = ["1", "2", "3", "sea", "user", 23];

search(list);
search(list2);
